<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_tbl".
 *
 * @property int $iduser
 * @property string $name
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_tbl';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iduser', 'name'], 'required'],
            [['iduser'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['iduser'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iduser' => 'Iduser',
            'name' => 'Name',
        ];
    }
}
