<?php

namespace app\controllers;

use yii\filters\Cors;
use yii\helpers\ArrayHelper;

class UserController extends \yii\rest\ActiveController

{
    public $modelClass = 'app\models\User';
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'PUT', 'HEAD', 'OPTIONS'],
                ],
            ],
        ], parent::behaviors());
    }
}
