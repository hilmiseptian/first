<?php

use yii\db\Migration;

/**
 * Class m211102_093354_seed_member_table
 */
class m211102_093354_seed_member_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insertFakeMembers();
    }
    
    private function insertFakeMembers() 
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $this->insert(
                'member',
                [
                    'name' => $faker->name
                ]
            );
        }
    }
    public function safeDown()
    {
        
    }
}
